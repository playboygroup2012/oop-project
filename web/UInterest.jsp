<%-- 
    Document   : UInterest
    Created on : Oct 11, 2012, 2:03:22 PM
    Author     : dung
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="U.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Interest</title>
    </head>
    <body>
        <div id="interest" class="contain" style="margin-top: 50px">
            <form action="UInterestss" method="post" name="form1">
                <table class="table">
                    <tr>
                        <td>Thể thao</td>
                        <td>
                            <select name="thethao" >
                                <option value=" " >                </option>
                                <option value="Bong ro">Bóng rổ</option>
                                <option value="Bong da">Bóng đá</option>
                                <option value="Bong ban">Bóng bàn</option>
                                <option value="Cau long">Cầu lông</option>
                                <option value="Boi">Bơi</option>
                            </select>
                        </td>
                    </tr>
                    <tr>

                    </tr>
                    <tr>
                        <td>
                            Âm nhạc 
                        </td>
                        <td>
                            <select name="amnhac">
                                <option value=" " >                </option>
                                <option value="Nhac rock">Nhạc rock</option>
                                <option value="Nhac tru tinh" >Nhạc trữ tình</option>
                                <option value="Nhac teen">Nhạc teen</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Điện ảnh
                        </td>
                        <td>
                            <select name="phim">
                                <option value=" " >                </option>
                                <option value="Phim kinh di">Phim kinh dị</option>
                                <option value="Phim hanh dong">Phim hành động</option>
                                <option value="Phim da su">Phim dã sử</option>
                                <option value="Phim tinh cam">Phim tình cảm</option>
                                <option value="Phim vien tuong">Phim viễn tưởng</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Nghệ thuật
                        </td>
                        <td>
                            <select name="nghethuat">
                                <option value=" " >                </option>
                                <option value="Nau an">Nấu ăn</option>
                                <option value="Khieu vu">Khiêu vũ</option>
                                <option value="Vu">Vẽ</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td> 
                            Các sở thích khác
                        </td>
                        <td>
                            <select name="khac">
                                <option value=" " >                </option>
                                <option value="Shopping">Shopping</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Xác nhận mật khẩu
                        </td>
                        <td>

                            <input type="password" name="pass">
                        </td>
                        <td>
                            ${requestScope.warning}
                        </td>
                    </tr>
                </table>
                        <input type="submit" class="btn" style="width: 100px" value="Xác nhận" >
            </form>
            <form action="U.jsp">
                <input class="btn" type="submit" style="width: 100px" value="Hủy bỏ">
            </form>
        </div>
       
    </body>
</html>
