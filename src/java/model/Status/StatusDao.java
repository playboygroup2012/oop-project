package model.Status;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.connect.ConnectDB;
import model.friend.FriendDao;

public class StatusDao implements IStatusDao {

    List<Integer> listFriend;

    public StatusDao() {
    }

    public boolean postStatusByIdUser(String stt, int idUser, int idUserPost) {
        try {
            // iinsert vao bang Status 
//            ContentStatus la :noi dung status 
//            IdUser la :status dang tren tuong cua user co id la IdUser
//            IdUserPostStaus la :id cua user da post status len tuong nha  IdUser 
            Connection conn = ConnectDB.CreateConnect();
            String insertStatus = "INSERT INTO Status(ContentStatus,IdUser,IdUserPostStaus,NumberLike) VALUES (? , ? , ?, ?)";

            PreparedStatement prps = conn.prepareCall(insertStatus);
            prps.setString(1, stt);
            prps.setInt(2, idUser);
            prps.setInt(3, idUserPost);
            prps.setInt(4, 0);
            prps.execute();
            return true;
        } //end
        catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }//end

    public String[] getContentStatusWithIdUser(int id) {
        String[] Status = new String[30];

        Connection conn = ConnectDB.CreateConnect();
        //lay ra tat ca cac status cua thang co id la id duoc truyen vao
        String query = "SELECT TOP 30 * FROM [Status] WHERE IdUser = ? ORDER BY IdStatus DESC";
        try {
            PreparedStatement prps = conn.prepareCall(query, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            prps.setInt(1, id);
            ResultSet rs = prps.executeQuery();
            for (int i = 0; i < 30; i++) {
                if (rs.next()) {
                    Status[i] = rs.getString("ContentStatus");
                }
            }

            conn.close();
            return Status;
        } //end class
        catch (Exception ex) {
            ex.printStackTrace();
            try {
                conn.close();
            } catch (SQLException ex1) {
                ex.printStackTrace();
            }

            return null;
        }//end
    }//end

    public int[] getIdUserPostStatusWithIdUser(int id) {
        int[] idUserPost = new int[30];

        Connection conn = ConnectDB.CreateConnect();
        //lay ra tat ca cac iduser cua nhung thang post status vao nha thang id(thang id co the post cho chinh no)
        String query = "SELECT TOP 30 IdUserPostStaus FROM [Status] WHERE IdUser = ? ORDER BY IdStatus DESC";
        try {
            PreparedStatement prps = conn.prepareCall(query, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            prps.setInt(1, id);
            ResultSet rs = prps.executeQuery();
            for (int i = 0; i < 30; i++) {
                if (rs.next()) {
                    idUserPost[i] = rs.getInt("IdUserPostStaus");
                }
            }

            conn.close();
            return idUserPost;
        } //end class
        catch (Exception ex) {
            ex.printStackTrace();
            try {
                conn.close();
            } catch (SQLException ex1) {
                ex.printStackTrace();
            }

            return null;
        }//end
    }//end

    public int[] getIdStatusWithIdUser(int id) {
        int[] IdStatus = new int[30];// tra ve 1 mang la 30 status moi nhat cua 1 user

        // method o tren lay ve noi dung 30 status moi nhat va method nay lay ve 30 id tuong ung voi 30 status do
        Connection conn = ConnectDB.CreateConnect();
        //lay ra tat ca cac id status cua user duoc truyen vao
        String query = "SELECT TOP 30 IdStatus FROM [Status] WHERE IdUser = ? ORDER BY IdStatus DESC";
        try {
            PreparedStatement prps = conn.prepareCall(query, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            prps.setInt(1, id);
            ResultSet rs = prps.executeQuery();
            for (int i = 0; i < 30; i++) {
                if (rs.next()) {
                    IdStatus[i] = rs.getInt("IdStatus");
                }
            }

            conn.close();
            return IdStatus;
        } //end class
        catch (Exception ex) {
            ex.printStackTrace();
            try {
                conn.close();
            } catch (SQLException ex1) {
                ex.printStackTrace();
            }

            return null;
        }//end
    }//end

    public int getNumberLikeStatus(int idstatus) {
        try {
            Connection conn = ConnectDB.CreateConnect();
            String selectLike = "SELECT NumberLike FROM [Status] WHERE IdStatus = ? ";
            PreparedStatement prps = conn.prepareCall(selectLike);
            prps.setInt(1, idstatus);
            ResultSet rs = prps.executeQuery();
            rs.next();
            int numberLike = rs.getInt("NumberLike");
            conn.close();

            return numberLike;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }//end catch
        return 0;
    }

    public int setNumberLikeStatus(int idstatus) {
        try {
            Connection conn = ConnectDB.CreateConnect();

            String updateLike = "UPDATE Status SET NumberLike = NumberLike+1 where IdStatus =" + idstatus;
            PreparedStatement prps = conn.prepareCall(updateLike);
            prps.execute();
            conn.close();
            return 0;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }//end catch
        return 0;
    }//end

    public boolean setUserLikeStatus(int idstatus, int idUser) {
        // insert vao csdl staus nay la user nao like
        try {
            Connection conn = ConnectDB.CreateConnect();
            String setUserLike = "INSERT INTO LikeStatus(IdStatus ,IdUser) VALUES (?,?)";
            PreparedStatement prps = conn.prepareCall(setUserLike);
            prps.setInt(1, idstatus);
            prps.setInt(2, idUser);
            prps.execute();
            conn.close();
            return true;
        } //end
        catch (SQLException ex) {
            Logger.getLogger(StatusDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }//end

    public boolean checkUserLikedStatus(int idstatus, int idUser) {
        // kiem cha xem idUser da like status co id la idstatus chua
        // neu roi tra ve true
        try {
            Connection conn = ConnectDB.CreateConnect();
            String checkLiked = "SELECT * FROM LikeStatus WHERE IdStatus = ? and IdUser = ?";
            PreparedStatement prps = conn.prepareCall(checkLiked);
            prps.setInt(1, idstatus);
            prps.setInt(2, idUser);
            ResultSet rs = prps.executeQuery();
            if (rs.next()) {
                conn.close();
                System.out.println("chua like");
                return false;
            }//end
            else {
                System.out.println("like roi");
            }//

        } //end
        catch (SQLException ex) {
            ex.printStackTrace();
        }//end
        return true;
    }//end

    public List listUserLikedStatus(int idstatus) {
        List<Integer> listUserLike = new ArrayList<Integer>();
        try {
            Connection conn = ConnectDB.CreateConnect();
            String listLiked = "SELECT * FROM LikeStatus WHERE IdStatus = ?";
            PreparedStatement prps = conn.prepareCall(listLiked);
            prps.setInt(1, idstatus);
            ResultSet rs = prps.executeQuery();
            while (rs.next()) {
                listUserLike.add(rs.getInt("IdUser"));
            }//end
            conn.close();
            return listUserLike;
        } //end
        catch (SQLException ex) {
            ex.printStackTrace();
        }//end
        return null;
    }//end

    public int setNumberUnLikeStatus(int idstatus) {
        try {
            Connection conn = ConnectDB.CreateConnect();
            String updateLike = "UPDATE Status SET NumberLike = NumberLike-1 where IdStatus =" + idstatus;
            PreparedStatement prps = conn.prepareCall(updateLike);
            prps.execute();
            conn.close();
            return 0;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }//end catch
        return 0;
    }//end

    public boolean setUserUnLikeStatus(int idstatus, int idUser) {
        try {
            Connection conn = ConnectDB.CreateConnect();
            String setUserUnLike = "DELETE FROM LikeStatus WHERE IdStatus = ? AND IdUser = ?";
            PreparedStatement prps = conn.prepareCall(setUserUnLike);
            prps.setInt(1, idstatus);
            prps.setInt(2, idUser);
            prps.execute();
            conn.close();
            return true;
        } //end
        catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;

    }//end

    // lay ve 30 id status cua no hoac cua ban be no
    @Override
    public List<Status> getStatusAtHome(int idUserSession) {
        // lay ve 1 list id la ban cua thang co id la idUser
        FriendDao friendDao = new FriendDao();
        listFriend = friendDao.listFriend(idUserSession);

        List<Status> listStatus = new ArrayList<Status>();
        String query = "SELECT * FROM [Status] ORDER BY IdStatus DESC";
        Connection conn = ConnectDB.CreateConnect();
        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(query);
//            System.out.println(isFriend(idUserSession, 2));
            while (rs.next()) {
                // neu status do la cua chinh no hoac la cua ban no thi add vao list
                if (isFriend(idUserSession, rs.getInt("IdUser"))||idUserSession==rs.getInt("IdUser")) {
                    Status status = new Status();
                    status.setIdStatus(rs.getInt("IdStatus"));
                    status.setIdUser(rs.getInt("IdUser"));
                    status.setImageStatus(rs.getString("ImageStatus"));
                    status.setNumberLike(rs.getInt("NumberLike"));
                    status.setContentStatus(rs.getString("ContentStatus"));
                    status.setTimePost(rs.getDate("TimePost"));
                    
                    if(listStatus.size()>=30)break;
                    listStatus.add(status);
                }
            }
            System.out.println("c"+listStatus.size());
//             System.out.println(listStatus.get(0).getContentStatus());
//             System.out.println(listStatus.get(1).getContentStatus());
//             System.out.println(listStatus.get(2).getContentStatus());
//             System.out.println(listStatus.get(3).getContentStatus());
            return listStatus;
        } catch (SQLException ex) {
            Logger.getLogger(StatusDao.class.getName()).log(Level.SEVERE, null, ex);
        }


        return null;
    }

    // kiem tra xem thang idUserSession co fai la ban cua thang idUser hay khong
    public boolean isFriend(int idUserSession, int idUser) {

        for (Integer i : listFriend) {
            if (i == idUser) {
//                System.out.println("la ban");
                return true;
            }
        }

//        System.out.println(listFriend.size());

//        System.out.println("khong la ban");
        return false;
    }

    public static void main(String[] args) {
        new StatusDao().getStatusAtHome(1);
//            new StatusDao().isFriend(1, 2);
    }//end main
}//end
