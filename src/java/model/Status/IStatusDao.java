/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.Status;

import java.util.List;

/**
 *
 * @author T2n
 */
public interface IStatusDao {
    public boolean postStatusByIdUser(String stt, int id, int id1);
    public String[] getContentStatusWithIdUser(int id) ;
    public int getNumberLikeStatus(int idstatus);
    public int setNumberLikeStatus(int idstatus);// update like vao csdl
    public int setNumberUnLikeStatus(int idstatus);// update unlike vao csdl
    public boolean setUserLikeStatus(int idstatus, int idUser);// insert user like status vao csdl
    public boolean setUserUnLikeStatus(int idstatus, int idUser);// delete user unlike trong csdl
    public boolean checkUserLikedStatus(int idstatus, int idUser);
    public List listUserLikedStatus(int idstatus);
    public List<Status> getStatusAtHome(int idUserSession);// lay ve 30 status moi nhat cua thang session hoac cua ban be no
    
}
