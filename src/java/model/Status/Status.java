/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.Status;

import java.sql.Date;

/**
 *
 * @author T2n
 */
public class Status {
    
    private int IdStatus;
    private int IdUser;
    private String ImageStatus;
    private String ContentStatus;
    private Date TimePost;
    private int NumberLike;

    /**
     * @return the IdStatus
     */
    public int getIdStatus() {
        return IdStatus;
    }

    /**
     * @param IdStatus the IdStatus to set
     */
    public void setIdStatus(int IdStatus) {
        this.IdStatus = IdStatus;
    }

    /**
     * @return the IdUser
     */
    public int getIdUser() {
        return IdUser;
    }

    /**
     * @param IdUser the IdUser to set
     */
    public void setIdUser(int IdUser) {
        this.IdUser = IdUser;
    }

    /**
     * @return the ImageStatus
     */
    public String getImageStatus() {
        return ImageStatus;
    }

    /**
     * @param ImageStatus the ImageStatus to set
     */
    public void setImageStatus(String ImageStatus) {
        this.ImageStatus = ImageStatus;
    }

    /**
     * @return the ContentStatus
     */
    public String getContentStatus() {
        return ContentStatus;
    }

    /**
     * @param ContentStatus the ContentStatus to set
     */
    public void setContentStatus(String ContentStatus) {
        this.ContentStatus = ContentStatus;
    }

    /**
     * @return the TimePost
     */
    public Date getTimePost() {
        return TimePost;
    }

    /**
     * @param TimePost the TimePost to set
     */
    public void setTimePost(Date TimePost) {
        this.TimePost = TimePost;
    }

    /**
     * @return the NumberLike
     */
    public int getNumberLike() {
        return NumberLike;
    }

    /**
     * @param NumberLike the NumberLike to set
     */
    public void setNumberLike(int NumberLike) {
        this.NumberLike = NumberLike;
    }
    
    
    
}
