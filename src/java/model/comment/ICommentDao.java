/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.comment;

/**
 *
 * @author T2n
 */
public interface ICommentDao {
    public String[] getAllContentCommentWithIdStatus(int id) ;
    public boolean postCommentWithIdStatusAndIdUser(String ContentComment,int IdStatus,int IdUserPost, int NumLike,String TimePost);
}
