/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.comment;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.connect.ConnectDB;

/**
 *
 * @author T2n
 */
public class CommentDao implements ICommentDao {

    public CommentDao() {
    }
// lay ve comment theo id cua status
    public String[] getAllContentCommentWithIdStatus(int id) {
        String[] Comment = new String[30];
        Connection conn = ConnectDB.CreateConnect();
        //lay ra tat ca (30)cac Comment  cua 1 status
        // 1 status co 1 id va co the co nhieu commen cho 1 status
        // ta se lay ra 30 comment moi nhat cho 1 status
        String query = "SELECT TOP 30 ContentComment FROM [Comment] WHERE IdStatus = ?";
        try {
            PreparedStatement prps = conn.prepareCall(query, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            prps.setInt(1, id);
            ResultSet rs = prps.executeQuery();
            for (int i = 0; i < 30; i++) {
                if (rs.next()) {
                    Comment[i] = rs.getString("ContentComment");
                }
            }

            conn.close();
            return Comment;
        } //end class
        catch (Exception ex) {
            ex.printStackTrace();
            try {
                conn.close();
            } catch (SQLException ex1) {
                ex.printStackTrace();
            }

            return null;
        }//end
    }//end
    
    
    
    // lay ve id cua user da comment cho status
    public int[] getAllIdUserWithIdStatus(int idStatus) {
        int[] idUserPostComment = new int[30];
        Connection conn = ConnectDB.CreateConnect();
        //lay ra tat ca (30)cac id cua user  comment cho 1 status
        // 1 status co 1 id va co the co nhieu commen cho 1 status
        // ta se lay ra 30 user moi nhat cho 1 status
        // ham nay se gan lien voi ham public String[] getAllContentCommentWithIdStatus(int id)
        // vi Idcomment se co IdUser tuong ung
        String query = "SELECT TOP 30 IdUser FROM [Comment] WHERE IdStatus = ?";
        try {
            PreparedStatement prps = conn.prepareCall(query, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            prps.setInt(1, idStatus);
            ResultSet rs = prps.executeQuery();
            for (int i = 0; i < 30; i++) {
                if (rs.next()) {
                    idUserPostComment[i] = rs.getInt("IdUser");
                }
            }

            conn.close();
            return idUserPostComment;
        } //end class
        catch (Exception ex) {
            ex.printStackTrace();
            try {
                conn.close();
            } catch (SQLException ex1) {
                ex.printStackTrace();
            }

            return null;
        }//end
    }//end
    

    // insert vao bang comment voi idStatus va nguoi da comment la IdUserPost(chinh la UserSession)
    public boolean postCommentWithIdStatusAndIdUser(String ContentComment, int IdStatus, int IdUserPost, int NumLike, String TimePost) {
        try {
            Connection conn = ConnectDB.CreateConnect();
            String insertComment = "INSERT INTO Comment(ContentComment,IdStatus,IdUser,NumberLike,TimeComment) VALUES (? , ? , ? ,? ,?)";

            PreparedStatement prps = conn.prepareCall(insertComment);
            prps.setNString(1, ContentComment);
            prps.setInt(2, IdStatus);
            prps.setInt(3, IdUserPost);
            prps.setInt(4, 0);
            prps.setString(5, "2012/1/1");
            prps.execute();
            return true;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    }

//    public static void main(String[] args) {
//        //        String[] contentComment = new CommentDao().getAllContentCommentWithIdStatus(26);
//        //        for (int i = 0; i < 10; i++) {
//        //            System.out.println(contentComment[i]);
//        //        }
//        //        new CommentDao().postCommentWithIdStatusAndIdUser("hehehehe", 1, 1, 1, null);
//        int[] IdUser = new CommentDao().getAllIdUserWithIdStatus(7);
//         for (int i = 0; i < 10; i++) {
//            System.out.println(IdUser[i]);
//        }
//    }//end main
}//end
