package model.comment;

import java.sql.Date;

public class Comment {
    
   private int IdStatus;
   private String ContentComment;
   private Date TimeComment;
   private int IdUser;
   private int NumberLike;

    /**
     * @return the IdStatus
     */
    public int getIdStatus() {
        return IdStatus;
    }

    /**
     * @param IdStatus the IdStatus to set
     */
    public void setIdStatus(int IdStatus) {
        this.IdStatus = IdStatus;
    }

    /**
     * @return the ContentComment
     */
    public String getContentComment() {
        return ContentComment;
    }

    /**
     * @param ContentComment the ContentComment to set
     */
    public void setContentComment(String ContentComment) {
        this.ContentComment = ContentComment;
    }

    /**
     * @return the TimeComment
     */
    public Date getTimeComment() {
        return TimeComment;
    }

    /**
     * @param TimeComment the TimeComment to set
     */
    public void setTimeComment(Date TimeComment) {
        this.TimeComment = TimeComment;
    }

    /**
     * @return the IdUser
     */
    public int getIdUser() {
        return IdUser;
    }

    /**
     * @param IdUser the IdUser to set
     */
    public void setIdUser(int IdUser) {
        this.IdUser = IdUser;
    }

    /**
     * @return the NumberLike
     */
    public int getNumberLike() {
        return NumberLike;
    }

    /**
     * @param NumberLike the NumberLike to set
     */
    public void setNumberLike(int NumberLike) {
        this.NumberLike = NumberLike;
    }
   
   
    
}
