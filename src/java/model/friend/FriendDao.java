package model.friend;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.connect.ConnectDB;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import model.user.User;

/**
 *
 * @author T2n
 */
public class FriendDao implements IFriendDao {

    public FriendDao() {
    }

    public boolean sendRequestFriend(int IdUserRequest, int idUser) {
        try {
            Connection conn = ConnectDB.CreateConnect();
            String insertRequest = "INSERT INTO RequestFriend(IdUser,IdUserRequest,Status) VALUES (?,?,?)";
            PreparedStatement prps = conn.prepareCall(insertRequest);
            prps.setInt(1, idUser);
            prps.setInt(2, IdUserRequest);
            prps.setInt(3, 0);
            prps.execute();
            conn.close();
            return true;
        } //end
        catch (SQLException ex) {

            ex.printStackTrace();
        }
        return false;

    }//end

    public boolean checkRequestFriend(int idUserRequest, int idUser) {
        // neu ton tai request idUserRequest gui cho idUser thi tra lai true
        // luc nay idUserRequest la idUserSession
        //idUser la idUserShow  => idUserSession da gui request cho idUserShow thi tra lai true
        try {
            Connection conn = ConnectDB.CreateConnect();
            String checkRequest = "SELECT * FROM RequestFriend WHERE IdUserRequest= ? and IdUser = ?";
            PreparedStatement prps = conn.prepareCall(checkRequest);
            prps.setInt(1, idUserRequest);
            prps.setInt(2, idUser);
            ResultSet rs = prps.executeQuery();
            if (rs.next()) {// neeu ton tai request tu idUserRequest cho idUser
                System.out.println("ton tai srequest");
                conn.close();
                return true;
            }//
            else {
                System.out.println("khong ton tai srequest");
                conn.close();
                return false;
            }

        } //end
        catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    }//end

    public List listReceiveRequestFriend(int idUser) {
        // lay ra tat ca cac IdUserRequest gui yeu cau ket ban toi idUser
        //(o day idUser chinh la idUserSession )
        // lay ra tat ca cac yeu cau ket ban voi idUserSession
        List<Integer> listReceiveRequestFriend = new ArrayList<Integer>();
        try {
            Connection conn = ConnectDB.CreateConnect();
            String checkRequest = "SELECT IdUserRequest FROM RequestFriend WHERE IdUser = ? and Status = 0";
            PreparedStatement prps = conn.prepareCall(checkRequest);
            prps.setInt(1, idUser);
            ResultSet rs = prps.executeQuery();

            while (rs.next()) {
                listReceiveRequestFriend.add(rs.getInt("IdUserRequest"));
            }//end while
            conn.close();
            return listReceiveRequestFriend;
        } catch (Exception e) {
            e.printStackTrace();
        }//end catch
        return null;
    }//end

    public List listSendRequestFriend(int idUser) {
        // lay ra tat ca cac IdUser ma idUserSession da gui yeu cau ket ban
        // idUser chinh la idUserSession
        List<Integer> listSendRequestFriend = new ArrayList<Integer>();
        try {
            Connection conn = ConnectDB.CreateConnect();
            String checkRequest = "SELECT IdUser FROM RequestFriend WHERE IdUserRequest = ? and Status = 0";
            PreparedStatement prps = conn.prepareCall(checkRequest);
            prps.setInt(1, idUser);
            ResultSet rs = prps.executeQuery();

            while (rs.next()) {
                listSendRequestFriend.add(rs.getInt("IdUser"));
            }//end while
            conn.close();
            return listSendRequestFriend;
        } catch (Exception e) {
            e.printStackTrace();
        }//end catch
        return null;
    }//end

    public boolean confirmRequestFrienf(int idUser, int idUserRequest) {
        try {
            // idUserSession chap nhan yeu cau cua idUserRequest
            Connection conn = ConnectDB.CreateConnect();
            String confirm = "UPDATE RequestFriend SET Status = 1 WHERE IdUser = ? and IdUserRequest = ?";
            PreparedStatement prps = conn.prepareCall(confirm);
            prps.setInt(1, idUser);
            prps.setInt(2, idUserRequest);
            prps.executeUpdate();
            conn.close();
            return true;
        } //end
        catch (Exception ex) {
            ex.printStackTrace();
        }//
        return false;
    }//end

    // tra ve 1 list id cac friend cua id session
    public List<Integer> listFriend(int idUser) {
        // truy xuat 2 lan vao bang request Friends
        // lan 1 lay ra tat ca cac ban be ma IdUserSession gui yeu cau va da duoc chap nhan
        // lan th 2 lay ra tat ca cac ban be cua IdUserSession ma IdUserSession da chap nhan 
        try {
            Connection conn = ConnectDB.CreateConnect();
            List<Integer> listFriend = new ArrayList<Integer>();
            String listFriendMeRequest = "SELECT * FROM RequestFriend WHERE IdUser = ? and Status = 1";
            String listFriendRequest = "SELECT * FROM RequestFriend WHERE IdUserRequest = ? and Status = 1";

            PreparedStatement prps1 = conn.prepareCall(listFriendMeRequest);
            prps1.setInt(1, idUser);
            ResultSet rs1 = prps1.executeQuery();

            while (rs1.next()) {
                listFriend.add(rs1.getInt("IdUserRequest"));
            }//end
            prps1.close();
            rs1.close();

            PreparedStatement prps2 = conn.prepareCall(listFriendRequest);
            prps2.setInt(1, idUser);
            ResultSet rs2 = prps2.executeQuery();
            while (rs2.next()) {
                listFriend.add(rs2.getInt("IdUser"));
            }//end
            prps2.close();
            rs2.close();

            conn.close();
            return listFriend;
        } //end
        catch (SQLException ex) {
            ex.printStackTrace();
        }//end
        return null;
    }//end

    public List<Integer> listFriendChung(List<Integer> l1, List<Integer> l2) {
        List<Integer> listFriendChung = new ArrayList<Integer>();
        for (int i = 0; i < l1.size(); i++) {
            for (int j = 0; j < l2.size(); j++) {
                if (l1.get(i) == l2.get(j)) {
                    listFriendChung.add(i);
                }//end
            }//end
        }//end
        return listFriendChung;
    }//end

    public int soFriendChung(int id1, int id2) {
        List<Integer> listFriendChung = new ArrayList();
        List<Integer> l1 = this.listFriend(id1);
        List<Integer> l2 = this.listFriend(id2);
        for (int i = 0; i < l1.size(); i++) {
            for (int j = 0; j < l2.size(); j++) {
                if (l1.get(i) == l2.get(j)) {
                    listFriendChung.add(i);
                }//end
            }//end
        }//end

        return listFriendChung.size();
    }//end

// Ham nay de load tat ca cac doi tuong Friend chua duoc ket ban
    public ArrayList<Friend> loadAll(int idUserIn) {
        Friend itIsMe = null;
        ArrayList<Friend> listAll = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection c = ConnectDB.CreateConnect();
        try {
            ps = c.prepareStatement("SELECT [User].*,Interest.*  FROM  [User],Interest  WHERE  [User].IdUser=Interest.IdUser");
            rs = ps.executeQuery();
            listAll = new ArrayList();
            while (rs.next()) {
                int iduser = rs.getInt("IdUser");
                int sex = rs.getInt("Sex");
                int dob = (int) rs.getDate("Dob").getYear();
                java.util.Date date = new java.util.Date();
                dob = date.getYear() + 1900 - dob;
                if (dob >= 13 && dob < 18) {
                    dob = 1;
                }
                if (dob >= 18 && dob < 25) {
                    dob = 2;
                }
                if (dob >= 25 && dob < 35) {
                    dob = 3;
                }
                if (dob > 35) {
                    dob = 4;
                }
                String living = null;
                living = rs.getString("Living");
                String job = null;
                job = rs.getString("Job");
                String sport = null;
                sport = rs.getString("Sport");
                String music = null;
                music = rs.getString("Music");
                String film = null;
                film = rs.getString("Film");
                String art = null;
                art = rs.getString("Art");
                String Different = null;
                Different = rs.getString("Different");
                String imageBanner = null;
                imageBanner = rs.getString("ImageBaner");
                String imageDisplay = null;
                imageDisplay = rs.getString("ImageDisplay");
                String userName = null;
                userName = rs.getString("UserName");
                String email = null;
                email = rs.getString("Email");
                Friend f = new Friend(iduser, sex, dob, living, job, sport, music, film, art, Different, 0, 0, userName, imageBanner, imageDisplay, email);
                if (idUserIn == f.getIdUser()) {
                    itIsMe = f;
                } else {
                    listAll.add(f);
                }
            }
            listAll.add(itIsMe);
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(FriendDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        List<Integer> listFriend = this.listFriend(idUserIn);
        List<Integer> listRemove = new ArrayList();
        if (listAll != null) {
            for (Integer integer : listFriend) {
                int dem = 0;
                for (Friend friend : listAll) {
                    if (integer == friend.getIdUser()) {
                        listRemove.add(dem);
                    }
                    dem++;
                }
            }
        }

        Collections.sort(listRemove, new Comparator<Integer>() {
            public int compare(Integer t, Integer t1) {
                return Integer.valueOf(t).compareTo(t1);
            }
        });
        int demm = 0;
        if (listRemove != null) {
            for (Integer integer : listRemove) {
                listAll.remove(integer - demm);
                demm++;
            }
        }



        return listAll;

    }

    public ArrayList<Friend> loadAllFull(ArrayList<Friend> list) {
        int size = list.size();
        for (Friend friend : list) {
            int soFriendChung = 0;
            soFriendChung = this.soFriendChung(friend.getIdUser(), list.get(size - 1).getIdUser());
            if (soFriendChung <= 5 && soFriendChung > 0) {
                soFriendChung = 1;
            }
            if (soFriendChung > 5 && soFriendChung <= 15) {
                soFriendChung = 2;
            }
            if (soFriendChung > 15 && soFriendChung <= 30) {
                soFriendChung = 3;
            }
            if (soFriendChung > 30) {
                soFriendChung = 4;
            }
            friend.setFriendChung(soFriendChung);
        }
        return list;
    }
// Ham nay de dua ra goi y ket ban

    public ArrayList<Friend> autoCheckForFindFriend(int idUserIn) {


        ArrayList<Friend> listF = this.loadAll(idUserIn);
        this.loadAllFull(listF);

        Friend f1 = null;
        f1 = listF.get(listF.size() - 1);
        listF.remove(listF.size() - 1);


        for (Friend friend : listF) {
            float check_Sex = 0;
            float point_Check_Sex = 0;
            float check_Age = 0;
            float point_Check_Age = 0;
            float check_Living = 0;
            float point_Check_Living = 0;
            float check_Job = 0;
            float point_Check_Job = 0;
            float check_Sport = 0;
            float point_Check_Sport = 0;
            float check_Music = 0;
            float point_Check_Music = 0;
            float check_Film = 0;
            float point_Check_Film = 0;
            float check_Art = 0;
            float point_Check_Art = 0;
            float check_Different = 0;
            float point_Check_Different = 0;
            float check_FriendChung = 0;
            float point_Check_FriendChung = 2;
// check age
            if (f1.getAge() != 0) {
                point_Check_Age = 2;
                if (f1.getAge() == friend.getAge()) {
                    check_Age = 2;
                }
            }

//check sex
            if (f1.getSex() != 0) {
                point_Check_Sex = (float) 1.5;
                if (f1.getSex() != friend.getSex()) {
                    check_Sex = (float) 1.5;
                }
            }

// check living
            if (f1.getLiving() != null) {
                point_Check_Living = 1;
                if (f1.getLiving().equals(friend.getLiving())) {
                    check_Living = 1;
                }
            }

// check job
            if (f1.getJob() != null) {
                point_Check_Job = 1;
                if (f1.getJob().equals(friend.getJob())) {
                    check_Job = 1;
                }
            }

//check interest sport
            if (f1.getInterestSport() != null) {
                point_Check_Sport = (float) 0.5;
                if (friend.getInterestSport() != null) {
                    check_Sport = (float) 0.25;
                    if (friend.getInterestSport().equals(f1.getInterestSport())) {
                        check_Sport = (float) 0.5;
                    }
                }
            }

//check interest music
            if (f1.getInterestMusic() != null) {
                point_Check_Music = (float) 0.5;
                if (friend.getInterestMusic() != null) {
                    check_Music = (float) 0.25;
                    if (friend.getInterestMusic().equals(f1.getInterestMusic())) {
                        check_Music = (float) 0.5;
                    }
                }
            }

//check interest film
            if (f1.getInterestFilm() != null) {
                point_Check_Film = (float) 0.5;
                if (friend.getInterestFilm() != null) {
                    check_Film = (float) 0.25;
                    if (friend.getInterestFilm().equals(f1.getInterestFilm())) {
                        check_Film = (float) 0.5;
                    }
                }
            }

//check interest art
            if (f1.getInterestArt() != null) {
                point_Check_Art = (float) 0.5;
                if (friend.getInterestArt() != null) {
                    check_Art = (float) 0.25;
                    if (friend.getInterestArt().equals(f1.getInterestArt())) {
                        check_Art = (float) 0.5;
                    }
                }
            }

//check interest different
            if (f1.getInterestDifferent() != null) {
                point_Check_Different = (float) 0.5;
                if (f1.getInterestDifferent().equals(friend.getInterestDifferent())) {
                    check_Different = (float) 0.5;
                }
            } else {
                check_Different = 0;
            }

// check interest friend chung
            if (f1.getFriendChung() == 1) {
                check_FriendChung = (float) 0.5;
            }
            if (f1.getFriendChung() == 2) {
                check_FriendChung = (float) 1;
            }
            if (f1.getFriendChung() == 3) {
                check_FriendChung = (float) 1.5;
            }
            if (f1.getFriendChung() == 4) {
                check_FriendChung = (float) 2;
            }
            friend.setDiem((check_Age + check_Art + check_Different + check_Film + check_FriendChung + check_Job + check_Living + check_Music + check_Sex + check_Sport) / (point_Check_Age + point_Check_Art + point_Check_Different + point_Check_Film + point_Check_FriendChung + point_Check_Job + point_Check_Living + point_Check_Music + point_Check_Sex + point_Check_Sport));
        }
        Collections.sort(listF, new Comparator<Friend>() {
            public int compare(Friend t, Friend t1) {
                return Float.valueOf(t1.getDiem()).compareTo(t.getDiem());
            }
        });


        // lay ve 10 friend co diem cao nhat.
        int i = 0;
        ArrayList<Friend> listF1 = new ArrayList();
        for (Friend friend : listF) {
            i++;
            if (i <= 10) {
                listF1.add(friend);
            }
        }
        return listF1;
    }

    public ArrayList<User> GetFriendOnline(int id) {
        ArrayList<User> list = new ArrayList<User>();
        User u = null;
        FriendDao fd = new FriendDao();

        List<Integer> listFriend = fd.listFriend(id);
        for (Integer integer : listFriend) {
            Connection c = ConnectDB.CreateConnect();
            PreparedStatement ps = null;
            ResultSet rs = null;
            try {
                ps = c.prepareStatement("SELECT * FROM [User] WHERE (IdUser=?)AND (Stt=1)");
                ps.setInt(1, integer);
                rs = ps.executeQuery();
                while (rs.next()) {
                    u = new User();
                    u.setIdUser(rs.getInt("IdUser"));
                    u.setEmail(rs.getString("Email"));
                    u.setUsername(rs.getString("UserName"));
                    u.setImageBanner(rs.getString("ImageBaner"));
                    list.add(u);
                }
                c.close();
            } catch (SQLException ex) {
                Logger.getLogger(FriendDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return list;
    }

    public  boolean checkOnline(int idUserSession, int id2) {
        ArrayList<User> listFriendOnline = this.GetFriendOnline(idUserSession);
        for (int i = 0; i < listFriendOnline.size(); i++) {
            if (id2 == listFriendOnline.get(i).getIdUser()) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        FriendDao fd = new FriendDao();
        ArrayList<User> GetFriendOnline = fd.GetFriendOnline(20);
        for (User user : GetFriendOnline) {
            System.out.println(user.getUsername());
        }

    }
}//end class

