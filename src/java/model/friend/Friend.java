/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.friend;

import java.sql.Date;

/**
 *
 * @author dung
 */
public class Friend {
    private int idUser;
    private int sex;
    private int age;
    private String living;
    private String job;
    private String interestSport;
    private String interestMusic;
    private String interestFilm;
    private String interestArt;
    private String interestDifferent;
    private int friendChung;
    private float diem;
    private String userName;
    private String imageBanner;
    private String imageDisplay;
    private String email;

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getLiving() {
        return living;
    }

    public void setLiving(String living) {
        this.living = living;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getInterestSport() {
        return interestSport;
    }

    public void setInterestSport(String interestSport) {
        this.interestSport = interestSport;
    }

    public String getInterestMusic() {
        return interestMusic;
    }

    public void setInterestMusic(String interestMusic) {
        this.interestMusic = interestMusic;
    }

    public String getInterestFilm() {
        return interestFilm;
    }

    public void setInterestFilm(String interestFilm) {
        this.interestFilm = interestFilm;
    }

    public String getInterestArt() {
        return interestArt;
    }

    public void setInterestArt(String interestArt) {
        this.interestArt = interestArt;
    }

    public String getInterestDifferent() {
        return interestDifferent;
    }

    public void setInterestDifferent(String interestDifferent) {
        this.interestDifferent = interestDifferent;
    }

    public int getFriendChung() {
        return friendChung;
    }

    public void setFriendChung(int friendChung) {
        this.friendChung = friendChung;
    }

    public float getDiem() {
        return diem;
    }

    public void setDiem(float diem) {
        this.diem = diem;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getImageBanner() {
        return imageBanner;
    }

    public void setImageBanner(String imageBanner) {
        this.imageBanner = imageBanner;
    }

    public String getImageDisplay() {
        return imageDisplay;
    }

    public void setImageDisplay(String imageDisplay) {
        this.imageDisplay = imageDisplay;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Friend() {
    }

    public Friend(int idUser, int sex, int age, String living, String job, String interestSport, String interestMusic, String interestFilm, String interestArt, String interestDifferent, int friendChung, float diem, String userName, String imageBanner, String imageDisplay, String email) {
        this.idUser = idUser;
        this.sex = sex;
        this.age = age;
        this.living = living;
        this.job = job;
        this.interestSport = interestSport;
        this.interestMusic = interestMusic;
        this.interestFilm = interestFilm;
        this.interestArt = interestArt;
        this.interestDifferent = interestDifferent;
        this.friendChung = friendChung;
        this.diem = diem;
        this.userName = userName;
        this.imageBanner = imageBanner;
        this.imageDisplay = imageDisplay;
        this.email = email;
    }
    
    
}
