/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.friend;

import java.util.List;

/**
 *
 * @author T2n
 */
public interface IFriendDao {
    public boolean sendRequestFriend(int idUserRequest,int idUser);// gui request tu idUserSession den idUsershow
    public boolean checkRequestFriend(int idUserRequest,int idUser);//check request tu idUserSession den idUsershow
    public List listReceiveRequestFriend(int idUser);// lay va list request den idUserSession
    public List listSendRequestFriend(int idUser);// lay va list request den idUserSession
    public boolean confirmRequestFrienf(int idUser, int idUser2);
    public List listFriend(int idUser);
    public List<Integer> listFriendChung(List<Integer> l1 , List<Integer> l2);
}
