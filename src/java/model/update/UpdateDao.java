/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.update;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.connect.ConnectDB;

/**
 *
 * @author dung
 */
public class UpdateDao implements IUpdateDao{

    public int changeUserName(Update ud) {
        String pass=null;
        try {
            Connection c=ConnectDB.CreateConnect();
            PreparedStatement ps=c.prepareStatement("select * from  [User] where IdUser=?");
            ps.setInt(1,ud.getIdUsers());
            ResultSet rs=ps.executeQuery();
            rs.next();
            pass=rs.getString("Password");
        } catch (SQLException ex) {
            Logger.getLogger(UpdateDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(pass.equals(ud.getPassWord())){
            try {
                Connection c1=ConnectDB.CreateConnect();
                PreparedStatement ps=c1.prepareStatement("UPDATE [User] SET UserName=? WHERE IdUser=?");
                ps.setString(1,ud.getUserName());
                ps.setInt(2, ud.getIdUsers());
                ps.executeUpdate();
                return 1;
                //update thanh cong thi tra ve gia tri 1
            } catch (SQLException ex) {
                Logger.getLogger(UpdateDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return 2;
        //nhap sai hoac chua nhap mat khau cu thi tra ve gia tri 2
    }

    public int changePass(Update ud) {
       String pass=null;
        try {
            Connection c=ConnectDB.CreateConnect();
            PreparedStatement ps=c.prepareStatement("select * from  [User] where IdUser=?");
            ps.setInt(1,ud.getIdUsers());
            ResultSet rs=ps.executeQuery();
            rs.next();
            pass=rs.getString("Password");
        } catch (SQLException ex) {
            Logger.getLogger(UpdateDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
            if(pass.equals(ud.getPassWord())){
                if(ud.getPassWordMoi1().equals(ud.getPassWordMoi2())){
                    try {
                        Connection c1=ConnectDB.CreateConnect();
                        PreparedStatement ps=c1.prepareStatement("UPDATE [User] SET Password=? WHERE IdUser=?");
                        ps.setString(1,ud.getPassWordMoi1());
                        ps.setInt(2, ud.getIdUsers());
                        ps.executeUpdate();
                        return 1;
                        //update thanh cong thi tra ve gia tri 1
                    } catch (SQLException ex) {
                        Logger.getLogger(UpdateDao.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else{
                    return 2;
                    //2 mat khau moi khong khop nhau thi tra ve gia tri 2
                    }
            }
        return 3;
        //chua nhap hoac nhap sai pass cu thi tra va gia tri 3 
    }

    public int changeHobby(Update ud) {
        String pass=null;
        try {
            Connection c=ConnectDB.CreateConnect();
            PreparedStatement ps=c.prepareStatement("select * from  [User] where IdUser=?");
            ps.setInt(1,ud.getIdUsers());
            ResultSet rs=ps.executeQuery();
            rs.next();
            pass=rs.getString("Password");
        } catch (SQLException ex) {
            Logger.getLogger(UpdateDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(pass.equals(ud.getPassWord())){
            try {
                Connection c1=ConnectDB.CreateConnect();
                PreparedStatement ps=c1.prepareStatement("UPDATE Interest SET Sport=?,Music=?,Film=?,Art=?,Different=?  WHERE IdUser=?");
                ps.setString(1,ud.getInterestSport());
                ps.setString(2,ud.getInterestMusic());
                ps.setString(3,ud.getInterestFilm());
                ps.setString(4,ud.getInterestArt());
                ps.setString(5,ud.getInterestDifferent());
                ps.setInt(6, ud.getIdUsers());
                ps.executeUpdate();
                return 1;
                //update thanh cong thi tra ve gia tri 1
            } catch (SQLException ex) {
                Logger.getLogger(UpdateDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return 2;
        //nhap sai hoac chua nhap mat khau cu thi tra ve gia tri 2
    }

    public int changeJob(Update ud) {
        String pass=null;
        try {
            Connection c=ConnectDB.CreateConnect();
            PreparedStatement ps=c.prepareStatement("select * from  [User] where IdUser=?");
            ps.setInt(1,ud.getIdUsers());
            ResultSet rs=ps.executeQuery();
            rs.next();
            pass=rs.getString("Password");
        } catch (SQLException ex) {
            Logger.getLogger(UpdateDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(pass.equals(ud.getPassWord())){
            try {
                Connection c1=ConnectDB.CreateConnect();
                PreparedStatement ps=c1.prepareStatement("UPDATE [User] SET Job=? WHERE IdUser=?");
                ps.setString(1,ud.getJob());
                ps.setInt(2, ud.getIdUsers());
                ps.executeUpdate();
                return 1;
                //update thanh cong thi tra ve gia tri 1
            } catch (SQLException ex) {
                Logger.getLogger(UpdateDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return 2;
        //nhap sai hoac chua nhap mat khau cu thi tra ve gia tri 2
    }

    public int changeLiving(Update ud) {
        String pass=null;
        try {
            Connection c=ConnectDB.CreateConnect();
            PreparedStatement ps=c.prepareStatement("select * from  [User] where IdUser=?");
            ps.setInt(1,ud.getIdUsers());
            ResultSet rs=ps.executeQuery();
            rs.next();
            pass=rs.getString("Password");
        } catch (SQLException ex) {
            Logger.getLogger(UpdateDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(pass.equals(ud.getPassWord())){
            try {
                Connection c1=ConnectDB.CreateConnect();
                PreparedStatement ps=c1.prepareStatement("UPDATE [User] SET Living=? WHERE IdUser=?");
                ps.setString(1,ud.getLiving());
                ps.setInt(2, ud.getIdUsers());
                ps.executeUpdate();
                return 1;
                //update thanh cong thi tra ve gia tri 1
            } catch (SQLException ex) {
                Logger.getLogger(UpdateDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return 2;
        //nhap sai hoac chua nhap mat khau cu thi tra ve gia tri 2
    }
    public static void main(String[] args) {
        UpdateDao udao=new UpdateDao();
        Update u=new Update(4, null, "122", null, null, null,"fff", null, null, null, null, null);
        int changeJob = udao.changeLiving(u);
        System.out.println(changeJob);
        
    }
}
