/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.update;

/**
 *
 * @author dung
 */
public interface IUpdateDao {
    public int changeUserName(Update ud);
    public int changePass(Update ud);
    public int changeHobby(Update ud);
    public int changeJob(Update ud);
    public int changeLiving(Update ud);
    
}
