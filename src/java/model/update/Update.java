/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.update;

/**
 *
 * @author dung
 */
public class Update {
    
    private int idUsers;
    private String userName;
    private String passWord;
    private String passWordMoi1;
    private String passWordMoi2;
    private String job;
    private String living;
    private String interestSport;
    private String interestMusic;
    private String interestFilm;
    private String interestArt;
    private String interestDifferent;

    public int getIdUsers() {
        return idUsers;
    }

    public void setIdUsers(int idUsers) {
        this.idUsers = idUsers;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getPassWordMoi1() {
        return passWordMoi1;
    }

    public void setPassWordMoi1(String passWordMoi1) {
        this.passWordMoi1 = passWordMoi1;
    }

    public String getPassWordMoi2() {
        return passWordMoi2;
    }

    public void setPassWordMoi2(String passWordMoi2) {
        this.passWordMoi2 = passWordMoi2;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getLiving() {
        return living;
    }

    public void setLiving(String living) {
        this.living = living;
    }

    public String getInterestSport() {
        return interestSport;
    }

    public void setInterestSport(String interestSport) {
        this.interestSport = interestSport;
    }

    public String getInterestMusic() {
        return interestMusic;
    }

    public void setInterestMusic(String interestMusic) {
        this.interestMusic = interestMusic;
    }

    public String getInterestFilm() {
        return interestFilm;
    }

    public void setInterestFilm(String interestFilm) {
        this.interestFilm = interestFilm;
    }

    public String getInterestArt() {
        return interestArt;
    }

    public void setInterestArt(String interestArt) {
        this.interestArt = interestArt;
    }

    public String getInterestDifferent() {
        return interestDifferent;
    }

    public void setInterestDifferent(String interestDifferent) {
        this.interestDifferent = interestDifferent;
    }

    public Update() {
    }

    public Update(int idUsers, String userName, String passWord, String passWordMoi1, String passWordMoi2, String job, String living, String interestSport, String interestMusic, String interestFilm, String interestArt, String interestDifferent) {
        this.idUsers = idUsers;
        this.userName = userName;
        this.passWord = passWord;
        this.passWordMoi1 = passWordMoi1;
        this.passWordMoi2 = passWordMoi2;
        this.job = job;
        this.living = living;
        this.interestSport = interestSport;
        this.interestMusic = interestMusic;
        this.interestFilm = interestFilm;
        this.interestArt = interestArt;
        this.interestDifferent = interestDifferent;
    }
    
}
