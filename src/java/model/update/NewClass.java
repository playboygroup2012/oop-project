/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.update;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.connect.ConnectDB;

/**
 *
 * @author dung
 */
public class NewClass {
    public static void main(String[] args) {
        try {
            Connection c=ConnectDB.CreateConnect();
            PreparedStatement ps=null;
            ps=c.prepareStatement("UPDATE [User] SET Sex=? WHERE IdUser=?");
            ps.setInt(1,2);
            ps.setInt(2,2);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(NewClass.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
