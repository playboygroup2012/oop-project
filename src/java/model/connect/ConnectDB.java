
package model.connect;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ConnectDB  {
    
    static Connection conn;

    public static Connection CreateConnect() {
        try {
            if (conn == null||conn.isClosed()) {
                try {
                    Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                    conn = DriverManager.getConnection("jdbc:sqlserver://localhost\\SQLEXPRESS:1234;user=sa;password=admin;database=oopbook");
                    System.out.println("connect ok");
                    return conn;
                } catch (Exception ex) {
                    ex.printStackTrace();
                    return null;
                }///end catch
            }// if
            return conn;
        } //end
        catch (SQLException ex) {
            Logger.getLogger(ConnectDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }//end
    
    public static void main(String[] args) {
        new ConnectDB().CreateConnect();
        
    }
}//end
