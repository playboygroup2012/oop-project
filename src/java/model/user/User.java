package model.user;

import java.sql.Date;

public class User {

    private int IdUser;
    private String Username;
    private String Password;
    private String Email;
    private String ImageBanner;
    private String ImageDisplay;
    private Date Dob;
    private String Job;
    private String Living;
    private int Status;
    private  int sex;
    static int idUserShow;
    
    

    /**
     * @return the IdUser
     */
    public int getIdUser() {
        return IdUser;
    }

    /**
     * @param IdUser the IdUser to set
     */
    public void setIdUser(int IdUser) {
        this.IdUser = IdUser;
    }

    /**
     * @return the Username
     */
    public String getUsername() {
        return Username;
    }

    /**
     * @param Username the Username to set
     */
    public void setUsername(String Username) {
        this.Username = Username;
    }

    /**
     * @return the Password
     */
    public String getPassword() {
        return Password;
    }

    /**
     * @param Password the Password to set
     */
    public void setPassword(String Password) {
        this.Password = Password;
    }

    /**
     * @return the Email
     */
    public String getEmail() {
        return Email;
    }

    /**
     * @param Email the Email to set
     */
    public void setEmail(String Email) {
        this.Email = Email;
    }

    /**
     * @return the ImageBanner
     */
    public String getImageBanner() {
        return ImageBanner;
    }

    /**
     * @param ImageBanner the ImageBanner to set
     */
    public void setImageBanner(String ImageBanner) {
        this.ImageBanner = ImageBanner;
    }

    /**
     * @return the ImageDisplay
     */
    public String getImageDisplay() {
        return ImageDisplay;
    }

    /**
     * @param ImageDisplay the ImageDisplay to set
     */
    public void setImageDisplay(String ImageDisplay) {
        this.ImageDisplay = ImageDisplay;
    }

    /**
     * @return the Dob
     */
    public Date getDob() {
        return Dob;
    }

    /**
     * @param Dob the Dob to set
     */
    public void setDob(Date Dob) {
        this.Dob = Dob;
    }

    /**
     * @return the Job
     */
    public String getJob() {
        return Job;
    }

    /**
     * @param Job the Job to set
     */
    public void setJob(String Job) {
        this.Job = Job;
    }

    /**
     * @return the Living
     */
    public String getLiving() {
        return Living;
    }

    /**
     * @param Living the Living to set
     */
    public void setLiving(String Living) {
        this.Living = Living;
    }

    /**
     * @return the Status
     */
    public int getStatus() {
        return Status;
    }

    /**
     * @param Status the Status to set
     */
    public void setStatus(int Status) {
        this.Status = Status;
    }

    /**
     * @return the sex
     */
    public int getSex() {
        return sex;
    }

    /**
     * @param sex the sex to set
     */
    public void setSex(int sex) {
        this.sex = sex;
    }


    
    
    
}//end class
