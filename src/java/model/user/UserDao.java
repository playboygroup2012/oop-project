/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.user;

import model.connect.ConnectDB;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author T2n
 */
public class UserDao implements IUserDao {

    public UserDao() {
    }

    // tra ve true neu ton tai 1 dong thoa man
    public boolean checkLogin(String email, String password) {
        Connection conn = ConnectDB.CreateConnect();
        String query = "SELECT * FROM [User] WHERE Email = ? and Password = ?";
        try {
            PreparedStatement prps = conn.prepareCall(query);
            prps.setString(1, email);
            prps.setString(2, password);
            ResultSet rs = prps.executeQuery();
            if (rs.next()) {// neu ton tai 1 dong thoa man  se tra ve true
                conn.close();
                System.out.println("ton tai user");
                return true;
            }//end if
            else {
                conn.close();
                System.out.println("khong ton tai user");
                return false;
            }//end else

        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }//end catch

    }//end

    public boolean checkAddAccount(String userName, String password, String email, String birthday, int sex) {
        Connection conn = ConnectDB.CreateConnect();
        String query = "SELECT * FROM [User] WHERE Email = ?";
        try {
            PreparedStatement prps = conn.prepareCall(query, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            prps.setString(1, email);
            ResultSet rs = prps.executeQuery();

            if (rs.next()) {// neu ton tai 1 dong thoa man  se tra ve false
                email = rs.getString("Email");
                conn.close();
                return false;
            }//end if
            else {
                conn.close();
                return true;
            }//end else F

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }//end

    public boolean addNew(String userName, String password, String email, String birthday, int sex) {
        String queryAddUser = "INSERT INTO [User]( Username, Password,Email, Dob,Sex,ImageBaner) values(?, ?, ?,?,?,?)";
//        String queryAddInterest = "INSERT INTO [Interest](IdUser) values(?)";
        Connection conn = ConnectDB.CreateConnect();
        try {
            PreparedStatement prps = conn.prepareCall(queryAddUser);
            prps.setString(1, userName);
            prps.setString(2, password);
            prps.setString(3, email);
            prps.setDate(4, new java.sql.Date(2012 / 1 / 1));
            prps.setInt(5, sex);
            prps.setString(6, "image/noavata.png");

            prps.executeUpdate();

            conn.close();
            
            addInterest(email);


        } catch (SQLException ex) {
            ex.printStackTrace();
        }//end
        return true;
    }//end

    void addInterest(String email) {
        int id = this.getIdUserWithEmail(email);
        System.out.println(id);
        Connection conn =  ConnectDB.CreateConnect();
        String queryAddInterest = "INSERT INTO [Interest](IdUser) values(?)";
        try {
            PreparedStatement prps = conn.prepareStatement(queryAddInterest);
            prps.setInt(1, id);
            prps.executeUpdate();
            conn.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }//end

    }

    public int getIdUserWithEmail(String email) {
        Connection conn = ConnectDB.CreateConnect();
        String query = "SELECT * FROM [User] WHERE Email = ?";
        try {
            PreparedStatement prps = conn.prepareCall(query);
            prps.setString(1, email);
            ResultSet rs = prps.executeQuery();
            // chi lay duoc duy nhat 1 ban ghi
            rs.next();
            int IdUser = rs.getInt("IdUser");
            conn.close();
            return IdUser;
        } //end class
        catch (Exception ex) {
            ex.printStackTrace();
            try {
                conn.close();
            } catch (SQLException ex1) {
                ex.printStackTrace();
            }

            return -1;
        }//end

    }//end

    public String getUsernameWithEmail(String email) {
        Connection conn = ConnectDB.CreateConnect();
        String query = "SELECT * FROM [User] WHERE Email = ?";
        try {
            PreparedStatement prps = conn.prepareCall(query);
            prps.setString(1, email);
            ResultSet rs = prps.executeQuery();
            // chi lay duoc duy nhat 1 ban ghi
            rs.next();
            String Username = rs.getString("Username");
            conn.close();
            return Username;
        } //end class
        catch (Exception ex) {
            ex.printStackTrace();
            try {
                conn.close();
            } catch (SQLException ex1) {
                ex.printStackTrace();
            }

            return null;
        }//end

    }//end

    public String getLinkImgBannerWithIdUser(int idUser) {
        Connection conn = ConnectDB.CreateConnect();
        String query = "SELECT * FROM [User] WHERE IdUser = ?";
        try {
            PreparedStatement prps = conn.prepareCall(query);
            prps.setInt(1, idUser);
            ResultSet rs = prps.executeQuery();
            // chi lay duoc duy nhat 1 ban ghi
            rs.next();
            String LinkImg = rs.getString("ImageBaner");
            conn.close();
            return LinkImg;
        } //end class
        catch (Exception ex) {
            ex.printStackTrace();
            try {
                conn.close();
            } catch (SQLException ex1) {
                ex.printStackTrace();
            }

            return null;
        }//end
    }//end

    public String getUsernameWithIdUser(int id) {
        Connection conn = ConnectDB.CreateConnect();
        String query = "SELECT * FROM [User] WHERE IdUser = ?";
        try {
            PreparedStatement prps = conn.prepareCall(query);
            prps.setInt(1, id);
            ResultSet rs = prps.executeQuery();
            // chi lay duoc duy nhat 1 ban ghi
            rs.next();
            String Username = rs.getString("Username");
            conn.close();
            return Username;
        } //end class
        catch (Exception ex) {
            ex.printStackTrace();
            try {
                conn.close();
            } catch (SQLException ex1) {
                ex.printStackTrace();
            }

            return null;
        }//end
    }//end

//   
    @Override
    public User getByEmail(String mail) {
        Connection conn = ConnectDB.CreateConnect();
        String query = "SELECT * FROM [User] WHERE Email = ?";
        try {
            PreparedStatement prps = conn.prepareStatement(query);
            prps.setString(1, mail);
            ResultSet rs = prps.executeQuery();
            // chi lay duoc duy nhat 1 ban ghi
            rs.next();
            User u = new User();
            u.setJob(rs.getString("Job"));
            u.setLiving(rs.getString("Living"));
            u.setDob(rs.getDate("Dob"));
            u.setSex(rs.getInt("Sex"));

            conn.close();
            return u;
        } //end class
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }//end

    }
    
        public User getByID(int id) {
        Connection conn = ConnectDB.CreateConnect();
        String query = "SELECT * FROM [User] WHERE IdUser = ?";
        try {
            PreparedStatement prps = conn.prepareStatement(query);
            prps.setInt(1, id);
            ResultSet rs = prps.executeQuery();
            // chi lay duoc duy nhat 1 ban ghi
            rs.next();
            User u = new User();
            u.setJob(rs.getString("Job"));
            u.setLiving(rs.getString("Living"));
            u.setDob(rs.getDate("Dob"));
            u.setSex(rs.getInt("Sex"));

            conn.close();
            return u;
        } //end class
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }//end

    }

    public static void main(String[] args) {
      
    }

    @Override
    public void updateImageBannerByEmail(String id, String link) {
        Connection conn = ConnectDB.CreateConnect();
        String query = "update [User] set ImageBaner = ? where IdUser = ? ";
        try {
            PreparedStatement prps = conn.prepareStatement(query);
            prps.setString(1, link);
            prps.setString(2, id);
            prps.execute();
            conn.close();
        } //end class
        catch (Exception ex) {
            ex.printStackTrace();
        }//end
    }
    public void UpDateStt(int id,int stt){
        Connection c = ConnectDB.CreateConnect();
        try {
                    PreparedStatement ps=c.prepareStatement("UPDATE [User] SET Stt=? WHERE IdUser=?");
                    ps.setInt(1,stt);
                    ps.setInt(2, id);
                    ps.executeUpdate();
                    c.close();
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
                   
    }
}//end class
