package model.user;

import java.sql.ResultSet;

public interface IUserDao {

    boolean checkLogin(String email, String pass);

    public boolean checkAddAccount(String userName, String password, String email, String dob, int sex);

    public boolean addNew(String userName, String password, String email, String birthday, int sex);
    
    public int getIdUserWithEmail(String email);
    
    public String getUsernameWithEmail(String email);
    
    public String getLinkImgBannerWithIdUser(int id);
    
    public String getUsernameWithIdUser(int id);
    
    public User getByEmail(String mail);
    
    public void updateImageBannerByEmail(String mail, String link);
}
