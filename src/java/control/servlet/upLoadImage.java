/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package control.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.user.UserDao;
import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileItemFactory;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author minhtuan
 */
public class upLoadImage extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            boolean isMunti = ServletFileUpload.isMultipartContent(request);
            if (!isMunti) {
                out.print("ngu");
            } else {
                FileItemFactory fif = new DiskFileItemFactory();
                ServletFileUpload upload = new ServletFileUpload(fif);
                List item = null;
                try {

                    item = upload.parseRequest(request);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Iterator iterator = item.iterator();
                Hashtable hashtable = new Hashtable();
                String fileName = null;

                while (iterator.hasNext()) {
                    FileItem fi = (FileItem) iterator.next();
                    if (fi.isFormField()) {
                        hashtable.put(fi.getFieldName(), fi.getString());
                    } else {

                        try {
                            String link = null;
                            String itemName = fi.getName();
                            fileName = itemName.substring(itemName.lastIndexOf("\\") + 1);
                            out.println("path : " + fileName + "<br/>");
                            String real = getServletContext().getRealPath("/" + "image\\" + fileName);
                            out.println("real : " + real);

                            link = real.replace("\\build", "");

                            out.print("<br/>link : " + link);

//                            File saveFile = new File("D:\\BackupData\\OOPBOOKS\\web\\image\\" + fileName);
                            File saveFile = new File(link);
                            
                            fi.write(saveFile);

                            UserDao userDao = new UserDao();
                            String id = request.getSession().getAttribute("IdUserSesion") + "";
                            userDao.updateImageBannerByEmail(id, "image/" + fileName);
                            response.sendRedirect("personal.jsp?IdUserShow="+id);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }//end
                }//end while
            }//end else
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }
// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
