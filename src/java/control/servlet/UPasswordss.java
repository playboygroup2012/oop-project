/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package control.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.update.Update;
import model.update.UpdateDao;

/**
 *
 * @author dung
 */
public class UPasswordss extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String pass = request.getParameter("pass");
        String pass1 = request.getParameter("pass1");
        String pass2 = request.getParameter("pass2");
        int id=(Integer) request.getSession().getAttribute("IdUserSesion");
        Update u=new Update(id, null, pass, pass1, pass2, null, null, null, null, null, null, null);
        UpdateDao ud=new UpdateDao();
        int change=ud.changePass(u);
        if(change==1){
            response.sendRedirect("U.jsp");
        }
        if(change==2){
            request.setAttribute("warning1","Mật khẩu mới không khớp !");
            request.getRequestDispatcher("UPassword.jsp").forward(request, response);
        }
        if(change==3){
           request.setAttribute("warning2","Mật khẩu xác nhận không đúng !");
           request.getRequestDispatcher("UPassword.jsp").forward(request, response); 
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
