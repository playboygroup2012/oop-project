/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package control.servlet;

import model.connect.ConnectDB;
import model.user.UserDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author T2n
 */
public class signin extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        try {
            String usernameSig = request.getParameter("usernameSig");
            String emailSig = request.getParameter("emailSig");
            String passwordSig = request.getParameter("passwordSig");
            String sexSig = request.getParameter("sexSig");// lay ve male or female
            String birthDay = request.getParameter("birthDay");// return year/month/day
            int sex = -1;
            if (sexSig.equals("Male")) {
                sex = 1;
            }//else
            else {
                sex = 0;
            }//end

            UserDao userDao = new UserDao();
            boolean checkAddAccount = userDao.checkAddAccount(usernameSig, passwordSig, emailSig, birthDay, sex);
            if (checkAddAccount) {
                userDao.addNew(usernameSig, passwordSig, emailSig, birthDay, sex);
                response.sendRedirect("registrySusses.jsp");
            }//end
            else {
                out.write("<h1>Registry not Susses</h1>");
            }//end
        } finally {
            out.close();
        }//end final
    }//end

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        response.getWriter().write("get sign in");
    }//end

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        response.getWriter().write("post sign in");
    }//end

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}//end class
