/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package control.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.comment.CommentDao;

/**
 *
 * @author T2n
 */
public class postComment extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            out.write("comment");
            String commentContent = request.getParameter("commentContent");
            String IdUserShow = request.getParameter("IdUserShow");
            int IdStatus = Integer.parseInt(request.getParameter("IdStatus"));
            int IdUserSesion = (Integer) request.getSession().getAttribute("IdUserSesion");
            //insert vao csdl (vao bang comment) 
            //commentContent : noi dung comment
            //IdStatus : day la comment cua status co id la IdStatus 
            //IdUserSesion : chi ra comment nay do user nao post ->comment nay chac chan do  IdUserSesion post
            
            CommentDao commentDao = new CommentDao();
            boolean postComment = commentDao.postCommentWithIdStatusAndIdUser(commentContent, IdStatus, IdUserSesion, 0, null);
            if(postComment){
                response.sendRedirect("personal.jsp?IdUserShow="+IdUserShow);
            }
            
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
