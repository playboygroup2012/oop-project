/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package control.filter;

import com.sun.net.httpserver.HttpServer;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author T2n
 */
public class finters implements Filter {

    public void init(FilterConfig filterConfig) throws ServletException {
//        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
//        throw new UnsupportedOperationException("Not supported yet.");

        HttpServletRequest requestFilter = (HttpServletRequest) request;
        HttpServletResponse responseFilter = (HttpServletResponse) response;
        Integer IdUserSesion = null;
        IdUserSesion = (Integer) requestFilter.getSession().getAttribute("IdUserSesion");
        
        if (IdUserSesion == null) {// neu chau co session thi cho dng nhap
            chain.doFilter(request, response);
        }//end if
        else {// neu co session roi thi lay session va lap tuc cho vao trang ca nhan
            IdUserSesion = (Integer) requestFilter.getSession().getAttribute("IdUserSesion");
            responseFilter.sendRedirect("personal.jsp?IdUserShow=" + IdUserSesion);
        }//end else

    }//end

    public void destroy() {
//        throw new UnsupportedOperationException("Not supported yet.");
    }
}
