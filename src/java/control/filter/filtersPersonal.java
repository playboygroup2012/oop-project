/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package control.filter;

import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author T2n
 */
public class filtersPersonal implements Filter {

    public void init(FilterConfig filterConfig) throws ServletException {
    }//end

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest requestFilter = (HttpServletRequest) request;
        HttpServletResponse responseFilter = (HttpServletResponse) response;

        Integer IdUserSesion = (Integer) requestFilter.getSession().getAttribute("IdUserSesion");

        if (IdUserSesion != null) {// neu co session thi cho dng nhap
            chain.doFilter(request, response);
//            responseFilter.sendRedirect("index.jsp");

        }//end if
        else {// neu co session roi thi lay session va lap tuc cho vao trang ca nhan
            responseFilter.sendRedirect("index.jsp");
        }//end else
    }//end

    public void destroy() {
    }//end
}
