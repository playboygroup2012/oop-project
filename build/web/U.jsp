 <%-- 
    Document   : U
    Created on : Oct 11, 2012, 2:00:56 PM
    Author     : dung
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="css/bootstrap-responsive.css" rel="stylesheet" media="screen">
        <link href="css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="css/bootstrap.css" rel="stylesheet" media="screen">
        <link href="css/update.css" rel="stylesheet" media="screen">
        <title>JSP Page</title>
    </head>
    <body class="background">
        <div class="menu">
            <ul class="nav nav-pills">
                <li class="active a">
                    <a href="home.jsp">Home</a>
                </li>
                <li class="a">
                    <a class="a" href=personal.jsp?IdUserShow=<%=request.getSession().getAttribute("IdUserSesion")%>>Profile</a>
                </li>
                <li>
                    <a class="a" href="UUsername.jsp">Username</a>
                </li>
                <li>
                    <a class="a" href="UPassword.jsp">Password</a>
                </li>
                <li>
                    <a class="a" href="UInterest.jsp">Interest</a>
                </li>
                <li>
                    <a class="a" href="ULiving.jsp">Living</a> 
                </li>
                <li>
                    <a class="a" href="UJob.jsp">Job</a>
                </li>
            </ul>
        </div>
    </body>
</html>
