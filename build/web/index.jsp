<%@page contentType="text/html" pageEncoding="UTF-8"%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/index.css" />
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <script language ="javascript" src="js/index.js"></script>
        <title>oopbook.com</title>
    </head>
    
    
    <body>
        <div id="container" style="font-family: cursive">
            <div id="top" style="border: 1px solid rgb(204, 204, 204);">
                <div  id="title">oopbook.com</div>

                <div id="formLogin"  style="color: white;width: 600;float: right;">
                    <form name="formLogin" action="login" method="get">
                        <table style="color: white;text-align: left">
                            <tr>
                                <td>Email</td>
                                <td>Password</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                    <input style="width: 180px;height: 25px" type="text" name="Email"/>
                                </td>
                                <td>
                                    <input style="width: 180px; height: 25px" type="password" name="Password" />
                                </td>
                                <td><input  class="btn" type="submit" onclick="return checklogin();" value="login" style="width: 50;height: 25; margin-top: -10px"/></td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>

            <div id="center">
                <div id="registry">
                    <form action="signin" method="post" name="formSignin">
                        <table style="width: 500;color: white;text-align: right;margin-top: 100;margin-left: 550;">

                            <tr>

                                <td>
                                    <h1>Sign Up</h1>
                                    <hr>

                                </td>
                                <td></td>
                                <td></td>

                            </tr>
                            <tr>
                                <td id="lblReg">UserName :</td>
                                <td><input id="txtReg" type="text" name="usernameSig" /></td>
                            </tr>
                            <tr>
                                <td id="lblReg">Email :</td>
                                <td><input id="txtReg" type="email" name="emailSig" /></td>
                            </tr>
                            <tr>
                                <td id="lblReg"> Password :</td>
                                <td><input id="txtReg" type="password" name="passwordSig" /></td>
                            </tr>
                            <tr>
                                <td id="lblReg">Re-Password :</td>
                                <td><input id="txtReg" type="password" name="rePasswordSig" /></td>
                            </tr>
                            <tr>
                                <td id="lblReg">I am :</td>
                                <td><select id="txtReg" name="sexSig">
                                        <option>Select-Gender</option>
                                        <option>Male</option>
                                        <option>Fe-Male</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td id="lblReg">Birthday</td>
                                <td><input name="birthDay" id="txtReg" type="date"></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><input class="" type="button" onclick="return checkSignIn();" value="Sign Up" style="width: 100;height: 40"></td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>

        </div>
    </body>
</html>
