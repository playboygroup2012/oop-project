<%@page import="model.user.UserDao"%>
<%@page import="model.friend.FriendDao"%>
<%@page import="java.util.List"%>
<%@page import="model.Status.StatusDao"%>
<%@page import="model.Status.Status"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="css/personal.css" />
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <title><%out.write("oopbook");%></title>
</head>
<body>
    <div id="topp">
        <div id="logop">oopbook.com</div>
        <div id="userp">
            <ul class="nav nav-pills">
                <li class="active">
                    <a href="home.jsp"><i class="icon-home"></i>Home</a>
                </li>
                <li>              
                    <%="<a id = \"ap\" href=\"personal.jsp?IdUserShow=" + (String.valueOf(session.getAttribute("IdUserSesion"))) + "\"><i class=\"icon-user\"></i>" +  String.valueOf(session.getAttribute("UsernameSesion")) + "</a>"%>
                </li>
                <li><a href="logout"><i class="icon-off"></i> Logout</a></li>
            </ul>
        </div>
    </div>

    <div id="containerp">
        <div id="centerp">
            <%
                StatusDao statusDao = new StatusDao();
                FriendDao friendDao = new FriendDao();
                UserDao userDao = new UserDao();

                // lay ve IdUserSession
                Integer IdUserSession = (Integer) request.getSession().getAttribute("IdUserSesion");

                // lay ve 30 status cua thang co id duoc truen vao
                List<Status> listStatus = statusDao.getStatusAtHome(IdUserSession);

                for (int i = 0; i < listStatus.size(); i++) {// hien thi toi da 30 status

            %>

            <!--<h1><%//= listStatus.get(i).getContentStatus()%></h1>-->

            <!-------------------------------------------------------------------------->
            <div >
                <div id ="titleStatus">
                    <img id ="imgStatus" <%="src=" + userDao.getLinkImgBannerWithIdUser(listStatus.get(i).getIdUser())%> width="50" height="50"<%=">"%></img>
                    <a <%="href=personal.jsp?IdUserShow=" + String.valueOf(listStatus.get(i).getIdUser()) + ">" + userDao.getUsernameWithIdUser(listStatus.get(i).getIdUser())%></a>
                </div>
                <!------end titleStatus--->

                <div id ="contentStatus">
                    <p><%= listStatus.get(i).getContentStatus()%></p>
                </div>
                <!----end contentStatus-->
                <div id ="likeStatus" style="width: 400;margin-left: 5; margin-bottom: 10;">
                    <%if (true) {%>
                    <%if (statusDao.checkUserLikedStatus(listStatus.get(i).getIdStatus(), IdUserSession)) {%>
                    <!--// neu like roi thi tu like se bi an di -->
                    <a <%="href=likeStatus?IdStatus=" + listStatus.get(i).getIdStatus() + "&IdUserSession=" + IdUserSession + "&IdUserShow=" + listStatus.get(i).getIdUser() + "&location=home" + ">"%><i class=" icon-thumbs-up"></i>like</a>   
                    <%} else {%>
                    <a <%="href=unLikeStatus?IdStatus=" + listStatus.get(i).getIdStatus() + "&IdUserSession=" + IdUserSession + "&IdUserShow=" + listStatus.get(i).getIdUser() + "&location=home" + ">"%><i class=" icon-thumbs-down"></i>unlike</a>   

                    <%}%>

                    <%if (statusDao.getNumberLikeStatus(listStatus.get(i).getIdStatus()) > 0) {%>
                    <a <%="href=listPersonLikeStatus.jsp?IdStatus=" + listStatus.get(i).getIdStatus() + "  style=\"margin-left: 20;\">"%><%=statusDao.getNumberLikeStatus(listStatus.get(i).getIdStatus())%> people like this</a>
                    <%}%>
                    <%}%>
                </div>
                <!----end likeStatus-->
                <hr/>
                <!------------------------------------------------------------------------------>
                <%

                    }//end for
                %>

            </div>
            <!----end comment-->

        </div>
        <!--end container-->
</body>