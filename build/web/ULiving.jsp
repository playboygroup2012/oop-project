<%-- 
    Document   : ULiving
    Created on : Oct 11, 2012, 2:05:23 PM
    Author     : dung
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="U.jsp" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Living</title>
    </head>
    <body>
        <div class="contain" style="margin-top: 50px">
            <form action="ULivingss" method="get">
                <table class="table">
                    <tr>
                    <td>Quê quán</td>
                    <td>
                        <select name="living">
                            <option value="Ha Tay">Hà Tây</option>
                            <option value="Bac Giang">Bắc Giang</option>
                            <option value="Bac Ninh">Bắc Ninh</option>
                            <option value="Ha Nam">Hà Nam</option>
                            <option value="Ha Noi">Hà Nội</option>
                            <option value="Ninh Binh">Ninh Bình</option>
                            <option value="Hoi Phong">Hải Phòng</option>
                            <option value="Thanh Hoa">Thanh Hóa</option>
                            <option value="Thai Binh">Thái Bình</option>
                        </select> 
                    </td>
                    </tr>
                    <tr>
                    <td>
                        Xác nhận mật khẩu
                    </td>
                    <td>
                        <input type="password" name="pass"/>
                    </td>
                    <td>${requestScope.warning}</td>
                    </tr>
                    <tr>
                    <td>
                        <input class="btn btn1" style="width: 100px" type="submit" value="Xác nhận"/>
                    </td>
                    </tr>
                </table>
            </form>
            <form action="U.jsp">
                <input class="btn btn1" type="submit" style="width: 100px"  value="Hủy bỏ"/>
            </form>
        </div>
    </body>
</html>
