
/****** Object:  Database [oopbook]    Script Date: 11/05/2012 10:40:01 ******/
CREATE DATABASE [oopbook] ON  PRIMARY 
USE [oopbook]
GO
/****** Object:  Table [dbo].[User]    Script Date: 11/05/2012 10:40:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[IdUser] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[Sex] [int] NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
	[ImageBaner] [nvarchar](100) NULL,
	[ImageDisplay] [nvarchar](100) NULL,
	[Dob] [datetime] NULL,
	[Stt] [int] NULL,
	[Job] [nvarchar](100) NULL,
	[Living] [nvarchar](100) NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[IdUser] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Status]    Script Date: 11/05/2012 10:40:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Status](
	[IdStatus] [int] IDENTITY(1,1) NOT NULL,
	[IdUser] [int] NOT NULL,
	[ImageStatus] [nvarchar](100) NULL,
	[ContentStatus] [nvarchar](2000) NULL,
	[TimePost] [datetime] NULL,
	[NumberLike] [int] NULL,
	[IdUserPostStaus] [int] NOT NULL,
	[IdUserLikeStaus] [int] NULL,
 CONSTRAINT [PK_Status1] PRIMARY KEY CLUSTERED 
(
	[IdStatus] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RequestFriend]    Script Date: 11/05/2012 10:40:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RequestFriend](
	[IdRequest] [int] IDENTITY(1,1) NOT NULL,
	[IdUser] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[IdUserRequest] [int] NOT NULL,
 CONSTRAINT [PK_RequestFriend] PRIMARY KEY CLUSTERED 
(
	[IdRequest] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Interest]    Script Date: 11/05/2012 10:40:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Interest](
	[IdInterest] [int] IDENTITY(1,1) NOT NULL,
	[IdUser] [int] NULL,
	[Sport] [nchar](50) NULL,
	[Music] [nchar](50) NULL,
	[Film] [nchar](50) NULL,
	[Art] [nchar](50) NULL,
	[Different] [nchar](50) NULL,
 CONSTRAINT [PK_Table_1_1] PRIMARY KEY CLUSTERED 
(
	[IdInterest] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Comment]    Script Date: 11/05/2012 10:40:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comment](
	[IdComment] [int] IDENTITY(1,1) NOT NULL,
	[IdStatus] [int] NOT NULL,
	[ContentComment] [nvarchar](2000) NOT NULL,
	[TimeComment] [datetime] NOT NULL,
	[IdUser] [int] NOT NULL,
	[NumberLike] [int] NOT NULL,
 CONSTRAINT [PK_Comment] PRIMARY KEY CLUSTERED 
(
	[IdComment] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LikeStatus]    Script Date: 11/05/2012 10:40:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LikeStatus](
	[IdLikeStaus] [int] IDENTITY(1,1) NOT NULL,
	[IdUser] [int] NOT NULL,
	[IdStatus] [int] NOT NULL,
 CONSTRAINT [PK_LikeStatus] PRIMARY KEY CLUSTERED 
(
	[IdLikeStaus] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK_Status1_User]    Script Date: 11/05/2012 10:40:03 ******/
ALTER TABLE [dbo].[Status]  WITH CHECK ADD  CONSTRAINT [FK_Status1_User] FOREIGN KEY([IdUser])
REFERENCES [dbo].[User] ([IdUser])
GO
ALTER TABLE [dbo].[Status] CHECK CONSTRAINT [FK_Status1_User]
GO
/****** Object:  ForeignKey [FK_RequestFriend_User]    Script Date: 11/05/2012 10:40:03 ******/
ALTER TABLE [dbo].[RequestFriend]  WITH CHECK ADD  CONSTRAINT [FK_RequestFriend_User] FOREIGN KEY([IdUser])
REFERENCES [dbo].[User] ([IdUser])
GO
ALTER TABLE [dbo].[RequestFriend] CHECK CONSTRAINT [FK_RequestFriend_User]
GO
/****** Object:  ForeignKey [FK_Interest_User]    Script Date: 11/05/2012 10:40:03 ******/
ALTER TABLE [dbo].[Interest]  WITH CHECK ADD  CONSTRAINT [FK_Interest_User] FOREIGN KEY([IdUser])
REFERENCES [dbo].[User] ([IdUser])
GO
ALTER TABLE [dbo].[Interest] CHECK CONSTRAINT [FK_Interest_User]
GO
/****** Object:  ForeignKey [FK_Comment_Status]    Script Date: 11/05/2012 10:40:03 ******/
ALTER TABLE [dbo].[Comment]  WITH CHECK ADD  CONSTRAINT [FK_Comment_Status] FOREIGN KEY([IdStatus])
REFERENCES [dbo].[Status] ([IdStatus])
GO
ALTER TABLE [dbo].[Comment] CHECK CONSTRAINT [FK_Comment_Status]
GO
/****** Object:  ForeignKey [FK_LikeStatus_Status]    Script Date: 11/05/2012 10:40:03 ******/
ALTER TABLE [dbo].[LikeStatus]  WITH CHECK ADD  CONSTRAINT [FK_LikeStatus_Status] FOREIGN KEY([IdStatus])
REFERENCES [dbo].[Status] ([IdStatus])
GO
ALTER TABLE [dbo].[LikeStatus] CHECK CONSTRAINT [FK_LikeStatus_Status]
GO
